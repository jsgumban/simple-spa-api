<?php

return [

    'providers' => [

        'google' => [
            'client_ids' => [
                env('GOOGLE_CLIENT_ID_1'),
                env('GOOGLE_CLIENT_ID_2'),
                env('GOOGLE_CLIENT_ID_3'),
            ]
        ]

    ]
];
