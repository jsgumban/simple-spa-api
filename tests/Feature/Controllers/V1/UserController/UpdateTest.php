<?php

namespace Tests\Feature\Controllers\V1\UserController;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /** @test */
    public function authenticatedUserCanUpdateUserDetails()
    {
        $user = create(User::class);

        $this->actingAs($user)
            ->putJson(route('users.update', ['user' => $user->id]), [
                'first_name' => $fn = $this->faker()->firstName(),
                'last_name' => $ln = $this->faker()->lastName(),
            ])
            ->assertOk()
            ->assertJsonStructure([
                'data' => ['id', 'first_name', 'last_name', 'full_name', 'email', 'phone_number']
            ])
            ->assertJson([
                'data' => [ 'id' => $user->id, 'first_name' => $fn, 'last_name' => $ln ]
            ]);

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'first_name' => $fn,
            'last_name' => $ln,
        ]);

        tap($user->fresh(), function ($user) use ($fn, $ln) {
            $this->assertEquals($user->first_name, $fn);
            $this->assertEquals($user->last_name, $ln);
        });
    }

    /** @test */
    public function firstNameIsRequiredToUpdateUserDetails()
    {
        $user = create(User::class);

        $this->actingAs($user)->putJson(route('users.update', ['user' => $user->id]), [
            'first_name' => '',
            'last_name' => $this->faker()->lastName(),
        ])
        ->assertStatus(422)
        ->assertJsonStructure([
            'message', 'errors' => ['first_name'],
        ]);
    }

    /** @test */
    public function lastNameIsRequiredToUpdateUserDetails()
    {
        $user = create(User::class);

        $this->actingAs($user)->putJson(route('users.update', ['user' => $user->id]), [
            'first_name' => $this->faker()->firstName(),
            'last_name' => '',
        ])
        ->assertStatus(422)
        ->assertJsonStructure([
            'message', 'errors' => ['last_name'],
        ]);
    }

    /** @test */
    public function unauthenticatedUserCannotUpdateUserDetails()
    {
        $user = create(User::class);

        $this->putJson(route('users.update', ['user' => $user->id]), [
            'first_name' => $this->faker()->firstName(),
            'last_name' => $this->faker()->lastName(),
        ])
        ->assertStatus(401);
    }

    public function otherUserCannotUpdateOtherUserDetails()
    {
        $user = create(User::class);
        $user2 = create(User::class);

        $this->actingAs($user2)
            ->putJson(route('users.update', ['user' => $user->id]), [
                'first_name' => $this->faker()->firstName(),
                'last_name' => $this->faker()->lastName(),
            ])
            ->assertStatus(403);
    }
}
