<?php

namespace Tests\Feature\Controllers\V1\UserController;

use Tests\TestCase;
use App\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class IndexTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authenticatedUserCanViewUserList()
    {
        create(User::class);
        create(User::class);
        $user = create(User::class);
        $token = JWTAuth::fromUser($user);

        $this->json('GET', route('users.index'), [], ['Authorization' => "Bearer $token"])
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    ['id', 'first_name', 'last_name', 'full_name', 'email', 'phone_number', 'created_at', 'updated_at'],
                ],
                'meta' => ['current_page', 'from', 'last_page', 'per_page', 'to', 'total'],
            ]);
    }

    /** @test */
    public function unauthenticatedUserMustNotViewUserList()
    {
        create(User::class);

        $this->json('GET', route('users.index'))
            ->assertStatus(401);
    }
}
