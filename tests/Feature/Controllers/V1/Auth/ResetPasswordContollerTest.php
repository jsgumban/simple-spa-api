<?php

namespace Tests\Feature\Controllers\V1\Auth;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\PasswordReset;

class ResetPasswordContollerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function userCanResetPassword()
    {
        $user = create(User::class);
        $pr = create(PasswordReset::class, ['email' => $user->email]);

        $this->json('POST', route('resetPassword'), [
            'email' => $user->email,
            'token' => $pr->token,
            'password' => $pw = 'new-password',
            'password_confirmation' => $pw,
        ])
            ->assertOk();

        // password_reset token must be remove from password_resets table
        $this->assertDatabaseMissing('password_resets', [
            'token' => $pr->token,
        ]);
        // user email must be remove form password_resets table
        $this->assertDatabaseMissing('password_resets', [
            'email' => $user->email,
        ]);

        // test login
        $token = auth()->attempt(['email' => $user->email, 'password' => $pw]);
        $this->assertNotEmpty($token);
        $this->assertAuthenticated();
        $this->assertAuthenticatedAs($user);
    }

    /** @test */
    public function emailMustBeValidatedOnPasswordReset()
    {
        $user = create(User::class);
        $pr = create(PasswordReset::class, ['email' => $user->email]);

        // not a valid email
        $this->json('POST', route('resetPassword'), [
            'email' => 'not_an_email',
            'token' => $pr->token,
            'password' => $pw = 'new-password',
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['email'],
            ]);

        // email does not exist on user table
        $this->json('POST', route('resetPassword'), [
            'email' => 'some.random@email.xxx',
            'token' => $pr->token,
            'password' => $pw = 'new-password',
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['email'],
            ]);

        // email does not exist password_resets table
        $user2 = create(User::class);
        $this->json('POST', route('resetPassword'), [
            'email' => $user2->email,
            'token' => $pr->token,
            'password' => $pw = 'new-password',
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['email'],
            ]);
    }

    /** @test */
    public function tokenMustBeValidatedOnPasswordReset()
    {
        $user = create(User::class);
        $pr = create(PasswordReset::class, ['email' => $user->email]);

        // token is required
        $r = $this->json('POST', route('resetPassword'), [
            'email' => $user->email,
            'token' => '',
            'password' => $pw = 'new-password',
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['token'],
            ]);

        // invalid token
        $this->json('POST', route('resetPassword'), [
            'email' => $user->email,
            'token' => 'some_random_token',
            'password' => $pw = 'new-password',
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['token'],
            ]);

        // token and email not match
        $pr = create(PasswordReset::class);
        $this->json('POST', route('resetPassword'), [
            'email' => $user->email,
            'token' => $pr->token,
            'password' => $pw = 'new-password',
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['token'],
            ]);
    }

    /** @test */
    public function passwordMustBeValidatedOnPasswordPeset()
    {
        $user = create(User::class);
        $pr = create(PasswordReset::class, ['email' => $user->email]);
        $pw = 'new-password';

        // password is required
        $r = $this->json('POST', route('resetPassword'), [
            'email' => $user->email,
            'token' => $pr->token,
            'password' => '',
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['password'],
            ]);

        // password must be 8 char above
        $this->json('POST', route('resetPassword'), [
            'email' => $user->email,
            'token' => $pr->token,
            'password' => '123',
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['password'],
            ]);

        // password and password confirmation must be equal
        $pr = create(PasswordReset::class);
        $this->json('POST', route('resetPassword'), [
            'email' => $user->email,
            'token' => $pr->token,
            'password' => $pw,
            'password_confirmation' => 'some-random-password',
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['password'],
            ]);
    }
}
