<?php

namespace Tests\Feature\Controllers\V1\Auth\AuthController;

use Tests\TestCase;
use App\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RefreshTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function userWithTokenCanRefreshToken()
    {
        $user = create(User::class);
        $token = JWTAuth::fromUser($user);

        $response = $this->json('POST', route('auth.refresh'), [], ['Authorization' => 'Bearer ' . $token])
            ->getData()
            ->data;

        $this->assertNotEquals($token, $response->access_token);
        $this->assertNotNull($response->access_token);
    }

    /** @test */
    public function userWithoutTokenCannotRefreshToken()
    {
        $response = $this->json('POST', route('auth.refresh'))->assertStatus(401);
    }
}
