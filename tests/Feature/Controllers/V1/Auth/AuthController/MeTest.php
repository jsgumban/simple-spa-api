<?php

namespace Tests\Feature\Controllers\V1\Auth\AuthController;

use Tests\TestCase;
use App\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authenticatedUserCanGetUserDetails()
    {
        $user = create(User::class);
        $token = JWTAuth::fromUser($user);

        $this->json('GET', route('auth.me'), [], ['Authorization' => "Bearer $token"])
            ->assertOk()
            ->assertJsonStructure([
                'data' => ['id', 'first_name', 'last_name', 'email', 'created_at', 'updated_at'],
            ])
            ->assertJson([
                'data' => [
                    'id'         => $user->id,
                    'first_name' => $user->first_name,
                    'last_name'  => $user->last_name,
                    'email'      => $user->email,
                ]
            ]);
    }

    /** @test */
    public function unauthenticatedUserCannotGetUserDetails()
    {
        $this->json('GET', route('auth.me'))->assertStatus(401);
    }
}
