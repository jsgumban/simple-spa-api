<?php

namespace Tests\Feature\Controllers\V1\Auth;

use Tests\TestCase;
use App\Models\User;
use App\Mail\VerifyEmail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Notifications\VerifyPhoneNumber;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Notifications\VerifyEmail as VerifyEmailNotification;

class RegisterControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /** @test */
    public function userCanRegisterWithEmail()
    {
        Notification::fake();
        Notification::assertNothingSent();

        $this->json('POST', route('register'), [
            'first_name'            => $fn = $this->faker->firstName,
            'last_name'             => $ln = $this->faker->lastName,
            'email'                 => $em = $this->faker->email,
            'password'              => $pw = 'password',
            'password_confirmation' => 'password',
        ])
            ->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'access_token',
                    'token_type',
                    'expires_in',
                    'user' => [
                        'id',
                        'first_name',
                        'last_name',
                        'email',
                        'created_at',
                        'updated_at'
                    ]
                ],
            ])
            ->assertJson([
                'data' => [
                    'user' => [
                        'first_name' => $fn,
                        'last_name' => $ln,
                        'email' => $em
                    ]
                ]
            ]);

        $this->assertDatabaseHas('users', [
            'first_name' => $fn,
            'last_name'  => $ln,
            'email'      => $em,
        ]);

        $user = User::whereEmail($em)->first();
        // token must be auto created
        $this->assertNotNull($user->email_verification_code);

        Notification::assertSentTo(
            $user,
            VerifyEmailNotification::class,
            function ($notification) use ($user) {
                $notifier = $notification->toMail($user);
                return $notifier->user === $user;
            }
        );
    }

    /** @test */
    public function userCanRegisterWithPhoneNumber()
    {
        Notification::fake();
        Notification::assertNothingSent();

        $this->json('POST', route('register'), [
            'first_name'            => $fn = $this->faker->firstName,
            'last_name'             => $ln = $this->faker->lastName,
            'phone_number'          => $pn = '639123456789',
            'password'              => $pw = 'password',
            'password_confirmation' => 'password',
        ])
            ->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'access_token',
                    'token_type',
                    'expires_in',
                    'user' => [
                        'id',
                        'first_name',
                        'last_name',
                        'phone_number',
                        'created_at',
                        'updated_at'
                    ]
                ],
            ])
            ->assertJson([
                'data' => [
                    'user' => [
                        'first_name' => $fn,
                        'last_name' => $ln,
                        'phone_number' => $pn
                    ]
                ]
            ]);

        $this->assertDatabaseHas('users', [
            'first_name'   => $fn,
            'last_name'    => $ln,
            'phone_number' => $pn,
        ]);

        $user = User::wherePhoneNumber($pn)->first();

        // token must be auto created
        $this->assertNotNull($user->phone_number_verification_code);

        Notification::assertSentTo($user, VerifyPhoneNumber::class);
    }

    /** @test */
    public function validateEmailOnUserRegistration()
    {
        $user = create(User::class);
        $fn = $this->faker->firstName;
        $ln = $this->faker->lastName;
        $pw = 'password';

        // no email field
        $this->json('POST', route('register'), [
            'first_name'            => $fn,
            'last_name'             => $ln,
            'password'              => $pw,
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['email'],
            ]);

        // empty email
        $this->json('POST', route('register'), [
            'first_name'            => $fn,
            'last_name'             => $ln,
            'email'                 => '',
            'password'              => $pw,
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['email'],
            ]);

        // invalid email format
        $this->json('POST', route('register'), [
            'first_name'            => $fn,
            'last_name'             => $ln,
            'email'                 => 'not_an_email',
            'password'              => $pw,
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['email'],
            ]);

        // invalid email length
        $this->json('POST', route('register'), [
            'first_name'            => $fn,
            'last_name'             => $ln,
            'email'                 => Str::random(256) . '@mail.com',
            'password'              => $pw,
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['email'],
            ]);

        // email already existed
        $this->json('POST', route('register'), [
            'first_name'            => $fn,
            'last_name'             => $ln,
            'email'                 => $user->email,
            'password'              => $pw,
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['email'],
            ]);
    }

    /** @test */
    public function validatePhoneOnUserRegistration()
    {
        $user = create(User::class, ['phone_number' => '6394512300755', 'email' => null]);
        $fn = $this->faker->firstName;
        $ln = $this->faker->lastName;
        $pw = 'password';

        // no phone_number field
        $this->json('POST', route('register'), [
            'first_name'            => $fn,
            'last_name'             => $ln,
            'password'              => $pw,
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['phone_number'],
            ]);

        // empty phone_number
        $this->json('POST', route('register'), [
            'first_name'            => $fn,
            'last_name'             => $ln,
            'phone_number'          => '',
            'password'              => $pw,
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['phone_number'],
            ]);

        // invalid phone_number format
        $this->json('POST', route('register'), [
            'first_name'            => $fn,
            'last_name'             => $ln,
            'phone_number'          => 'not_a_valid_phone_number',
            'password'              => $pw,
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['phone_number'],
            ]);

        // invalid phone_number length
        $this->json('POST', route('register'), [
            'first_name'            => $fn,
            'last_name'             => $ln,
            'phone_number'          => Str::random(256),
            'password'              => $pw,
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['phone_number'],
            ]);

        // phone_number already existed
        $this->json('POST', route('register'), [
            'first_name'            => $fn,
            'last_name'             => $ln,
            'phone_number'          => $user->phone_number,
            'password'              => $pw,
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['phone_number'],
            ]);
    }

    /** @skip */
    public function validateFirstNameOnUserRegistration()
    {
        $user = create(User::class);
        $em = $this->faker->email;
        $ln = $this->faker->lastName;
        $pw = 'password';

        // no first name field
        $this->json('POST', route('register'), [
            'last_name'             => $ln,
            'email'                 => $em,
            'password'              => $pw,
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['first_name'],
            ]);

        // empty first name
        $this->json('POST', route('register'), [
            'first_name'            => '',
            'last_name'             => $ln,
            'email'                 => $em,
            'password'              => $pw,
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['first_name'],
            ]);

        // invalid first name length
        $this->json('POST', route('register'), [
            'first_name'            => Str::random(256),
            'last_name'             => $ln,
            'email'                 => $em,
            'password'              => $pw,
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['first_name'],
            ]);
    }

    /** @skip */
    public function validateLastNameOnUserRegistration()
    {
        $user = create(User::class);
        $em = $this->faker->email;
        $fn = $this->faker->firstName;
        $pw = 'password';

        // no last name field
        $this->json('POST', route('register'), [
            'first_name'            => $fn,
            'email'                 => $em,
            'password'              => $pw,
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['last_name'],
            ]);

        // empty last name
        $this->json('POST', route('register'), [
            'last_name'            => '',
            'first_name'             => $fn,
            'email'                 => $em,
            'password'              => $pw,
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['last_name'],
            ]);

        // invalid last name length
        $this->json('POST', route('register'), [
            'last_name'            => Str::random(256),
            'first_name'             => $fn,
            'email'                 => $em,
            'password'              => $pw,
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['last_name'],
            ]);
    }

    /** @test */
    public function validatePasswordOnUserRegistration()
    {
        $user = create(User::class);
        $em = $this->faker->email;
        $fn = $this->faker->firstName;
        $ln = $this->faker->lastName;
        $pw = 'password';

        // no password field
        $this->json('POST', route('register'), [
            'first_name'            => $fn,
            'last_name'             => $ln,
            'email'                 => $em,
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['password'],
            ]);

        // empty password
        $this->json('POST', route('register'), [
            'last_name'             => $ln,
            'first_name'            => $fn,
            'email'                 => $em,
            'password'              => '',
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['password'],
            ]);

        // invalid password length
        $this->json('POST', route('register'), [
            'first_name'            => $fn,
            'last_name'             => $ln,
            'email'                 => $em,
            'password'              => Str::random(7),
            'password_confirmation' => $pw,
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['password'],
            ]);

        // password confirmation fail
        $this->json('POST', route('register'), [
            'first_name'            => $fn,
            'last_name'             => $ln,
            'email'                 => $em,
            'password'              => $pw,
            'password_confirmation' => 'not_equal_to_password',
        ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message', 'errors' => ['password'],
            ]);
    }
}
