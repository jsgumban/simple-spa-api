<?php

namespace App\Rules;

use LVR\Phone\Phone;
use Illuminate\Contracts\Validation\Rule;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\RFCValidation;
use Egulias\EmailValidator\Validation\DNSCheckValidation;
use Egulias\EmailValidator\Validation\SpoofCheckValidation;
use Egulias\EmailValidator\Validation\MultipleValidationWithAnd;

class ValidEmailOrPhoneNumber implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->isEmail($value) || $this->isPhone($value)) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.valid_email_or_phone_number');
    }

    /**
     * Check if the given string value is a valid email
     * @param string $value The email to check
     * @return bool is it valid email
     */
    private function isEmail(string $value): bool
    {
        return (new EmailValidator())->isValid(
            $value,
            new MultipleValidationWithAnd([
                new RFCValidation(),
                new DNSCheckValidation(),
                new SpoofCheckValidation(),
            ])
        ) && filter_var($value, FILTER_VALIDATE_EMAIL) !== false;
    }

    /**
     * Checks through all validation methods to verify it is in a
     * phone number format of some type
     * @param  string  $value The phone number to check
     * @return bool is it valid phone number ?
     */
    private function isPhone(string $value): bool
    {
        return  $this->isE164($value) || $this->isNANP($value) || $this->isDigits($value);
    }

    /**
     * Format example 5555555555, 15555555555
     * @param  string  $value The phone number to check
     * @return boolean is it correct format?
     */
    private function isDigits(string $value): bool
    {
        $conditions = [];
        $conditions[] = strlen($value) >= 10;
        $conditions[] = strlen($value) <= 16;
        $conditions[] = preg_match("/[^\d]/i", $value) === 0;
        return (bool) array_product($conditions);
    }
    /**
     * Format example +22 555 555 1234, (607) 555 1234, (022607) 555 1234
     * @param string  $value The phone number to check
     * @return bool is it correct format?
     */
    private function isE123(string $value): bool
    {
        return preg_match('/^(?:\((\+?\d+)?\)|\+?\d+) ?\d*(-?\d{2,3} ?){0,4}$/', $value) === 1;
    }
    /**
     * Format example +15555555555
     * @param  string  $value The phone number to check
     * @return bool is it correct format?
     */
    private function isE164(string $value): bool
    {
        $conditions = [];
        $conditions[] = strpos($value, "+") === 0;
        $conditions[] = strlen($value) >= 9;
        $conditions[] = strlen($value) <= 16;
        $conditions[] = preg_match("/[^\d+]/i", $value) === 0;
        return (bool) array_product($conditions);
    }
    /**
     * Format examples: (555) 555-5555, 1 (555) 555-5555, 1-555-555-5555, 555-555-5555, 1 555 555-5555
     * https://en.wikipedia.org/wiki/National_conventions_for_writing_telephone_numbers#United_States.2C_Canada.2C_and_other_NANP_countries
     * @param  string $value The phone number to check
     * @return bool is it correct format?
     */
    private function isNANP(string $value): bool
    {
        $conditions = [];
        $conditions[] = preg_match("/^(?:\+1|1)?\s?-?\(?\d{3}\)?(\s|-)?\d{3}-\d{4}$/i", $value) > 0;
        return (bool) array_product($conditions);
    }
}
