<?php

namespace App\Http\Requests;

use App\Models\User;
use App\Rules\ValidPhoneNumber;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class RegisterUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'   => ['required', 'string', 'max:255', 'unique:users'],
            'email'        => [
                Rule::requiredIf(empty($this->phone_number)),
                'bail',
                'email',
                'max:255',
                'unique:users'
            ],
            'phone_number' => [
                Rule::requiredIf(empty($this->email)),
                'bail',
                'nullable',
                new ValidPhoneNumber,
                'unique:users'
            ],
            'first_name'   => ['required', 'string', 'max:255'],
            'last_name'    => ['required', 'string', 'max:255'],
            'password'     => ['required', 'string', 'min:8', 'confirmed'],
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        if ($this->phone_number) {
            $this->merge([
                'phone_number' => User::cleanPhoneNumber($this->phone_number)
            ]);
        }
    }
}
