<?php

namespace App\Http\Controllers\V1\Auth;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\PasswordReset;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\CheckResetPasswordTokenRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Update user password and remove all password_reset associated to user
     *
     * @param ResetPasswordRequest $request
     * @return void
     */
    public function __invoke(ResetPasswordRequest $request)
    {
        DB::transaction(function () use ($request) {
            // Change user password
            $user = User::where('email', $request->email)->first();
            $user->password = Hash::make($request->password);
            $user->save();

            // Remove all password reset entry for this email
            PasswordReset::whereEmail($user->email)->delete();
        });

        return response()->json([
            'message' => trans('passwords.reset'),
        ]);
    }

    /**
     * Check reset password token
     *
     * @param CheckResetPasswordTokenRequest $request
     * @return JsonResource
     */
    public function checkToken(CheckResetPasswordTokenRequest $request): JsonResource
    {
        $passwordReset = PasswordReset::where('email', $request->email)
            ->where('token', $request->token)->first();

        $passwordReset->makeVisible(['token']);

        return new JsonResource($passwordReset);
    }
}
