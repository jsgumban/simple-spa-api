<?php

namespace App\Http\Controllers\V1\Auth;

use Illuminate\Http\Request;
use BenSampo\Enum\Rules\EnumValue;
use App\Models\LinkedAccount;
use App\Lib\Oauth\Providers\Oauth\Oauth;
use App\Http\Resources\UserResource;
use App\Http\Controllers\Controller;
use App\Enums\SocialiteProvider;
use App\Lib\Oauth\User as OauthUser;
use App\Models\User;

class SocialAuthController extends Controller
{
    public function __invoke(Request $request)
    {
        $data = $request->validate([
            'token' => 'required',
            'provider' => ['required', new EnumValue(SocialiteProvider::class)],
        ]);

        $provider = Oauth::provider($data['provider']);
        $oauthUser = $provider->userFromToken($data['token']);

        // get link account
        $linkedAccount = LinkedAccount::where('provider_id', $oauthUser->getId())
            ->where('provider', $data['provider'])->first();

        if ($linkedAccount) {
            $user = $linkedAccount->user;
        } else {
            // register the suer
            $user = $this->registerUserFromOauth($oauthUser, $data['provider']);
        }

        $token = auth()->login($user);
        return $this->respondWithToken($token, new UserResource($user));
    }

    private function registerUserFromOauth(OauthUser $oauthUser, string $provider): User
    {

        $user = User::where('email', $oauthUser->getEmail())->first();

        if (!$user) {
            // Create new user
            $user = User::create([
                'email' => $oauthUser->getEmail(),
                'first_name' => $oauthUser->getFirstName(),
                'last_name' => $oauthUser->getLastName(),
                'email_verified_at' => now(),
            ]);

            $fileName = md5(uniqid('AVATAR' . $user->id, true));
            // Add avatar
            $user->addMediaFromUrl($oauthUser->getAvatar())
                ->usingName($fileName)
                ->toMediaCollection('avatar');
        }

        $user->linkedAccounts()->create([
            'provider' => $provider,
            'provider_id' => $oauthUser->getId(),
        ]);

        return $user;
    }
}
